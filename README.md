# POV en Arduino

## Instrucciones Software

1. No olvides poner el `Fonts.h` en tu carpeta de libraries de arduino que suele ser `\Users\%USER%\Documents\Arduino\libraries`


## Instrucciones Hardware

1. Revisa el .ino para conectar los leds en los pines correspondientes.
2. Recuerda poner una resistencia de 220 Ohms por LED y poner todos los LEDS en hilera, no demasiado separados.
3. Recomiendo poner una batería de 9V conectada directamente el arduino para no depender de una computadora.


