//Aqui se incluye el alfabeto y esas cosas cool!
#include "font.h"

// Pines del arduino UNO
const int LEDpins[] = {6,7,8,9,10,11,12};
 
const int charHeight = sizeof(LEDpins);
const int charWidth = 5;


//Cada mensaje se muestra 50 veces antes de cambiar. 
char linea1[] = "HOLA";
char linea2[] = "MUNDO";
  
 
void setup()
{
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
}
 
void loop()
{
  for (int n = 0; n <=50 ; n ++)
   {
     for (int k=0; k<sizeof(linea1); k++)
     {
       printLetter(linea1[k]);
     }
   }

  for (int n = 0; n <=50 ; n ++)
   {
     for (int k=0; k<sizeof(linea2); k++)
     {
       printLetter(linea2[k]);
     }
   }   
}
 
void printLetter(char ch)
{
  // make sure the character is within the alphabet bounds (defined by the font.h file)
  // if it's not, make it a blank character
   
  if (ch < 32 || ch > 126)
  {
    ch = 32;
  }
  // subtract the space character (converts the ASCII number to the font index number)
  ch -= 32;
  // step through each byte of the character array
  for (int i=0; i<charWidth; i++)
  {
    byte b = font[ch][i];
    // bit shift through the byte and output it to the pin
    for (int j=0; j<charHeight; j++)
    {
      digitalWrite(LEDpins[j], !!(b & (1 << j)));
    }
    // space between columns
 
   delayMicroseconds(900);
 }
 
 //clear the LEDs
 digitalWrite(6, LOW);
 digitalWrite(7, LOW);
 digitalWrite(8, LOW);
 digitalWrite(9, LOW);
 digitalWrite(10, LOW);
 digitalWrite(11, LOW);
 digitalWrite(12, LOW);
 
 // space between letters
 delayMicroseconds(2500); //2500
 
 }
